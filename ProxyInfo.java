package me.unreal.proxyinfo;

import net.md_5.bungee.api.plugin.Plugin;

public class ProxyInfo extends Plugin {
	/*
	 * Made by UnrealK9
	 * on 2020.09.03.
	 */
	@Override
	public void onEnable() {
		this.getProxy().getPluginManager().registerCommand(this, new ProxyInformCmd(this));
	}
	
}
