package me.unreal.proxyinfo;

import java.util.ArrayList;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class ProxyInformCmd extends Command {
	/*
	 * Made by UnrealK9
	 * on 2020.09.03.
	 */
	private ProxyInfo plugin;
	public ProxyInformCmd(ProxyInfo plugin) {
		super("proxyinfo","proxyinfo.admin",new String[]{"pinfo","proxyinf","proxyi"});
		this.plugin=plugin;
	}
	@Override
	public void execute(final CommandSender md5ProxySenderApplicable, final String[] fqzmbrdmw4drvu7q8h276q) {
		if(!md5ProxySenderApplicable.hasPermission("proxyinfo.admin")&&!md5ProxySenderApplicable.getGroups().contains("admin")) md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§cNincs jogod ehhez!"));
		else{
			if(fqzmbrdmw4drvu7q8h276q.length==0) {
				md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§5Proxy név: §7" + this.plugin.getProxy().getName()));
				md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§5Verzió: §7" + this.plugin.getProxy().getVersion()));
				md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§5Online: §7" + this.plugin.getProxy().getOnlineCount()));
				md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§5Szerverek: §7" + this.plugin.getProxy().getServers().toString()));
			}else if(fqzmbrdmw4drvu7q8h276q.length>=1) {
				if(fqzmbrdmw4drvu7q8h276q[0].equalsIgnoreCase("list")||fqzmbrdmw4drvu7q8h276q[0].equalsIgnoreCase("playerlist")) {
					final ArrayList<String> pList=new ArrayList<String>();
					this.plugin.getProxy().getPlayers().forEach(player->{pList.add(player.getName());});
					md5ProxySenderApplicable.sendMessage(TextComponent.fromLegacyText("§5Játékosok: §a" + String.join("§f, §a", pList)));
				}
			}
		}
	}
}
